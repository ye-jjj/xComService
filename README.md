## xCom Service 

是一个和 xPort 交互，以及将 xPort 部分二进制接口封装成 xRPC 接口的系统级别的DXC。

当前的功能有：

- 建立 xPort 间的连接
- 加载/卸载 应用组件
- 提供查询当前 xPort 所加载的应用组件信息
- 负责同步本地加载/卸载的应用组件到所有和自身连接的 xPort 节点。


## 使用说明

使用 xComBuilder( 安装方法参看 [xCom Builder](https://gitee.com/DXmesh/xComBuilder) 项目 )

在控制台中，将目录切换到本项目后,

1.  先编译项目，命令如下：

    ```sh
    dxc build
    ```

2.  打包项目，命令如下：

    ```sh
    dxc pack
    ```
  执行完打包命令后，会在当前目录生成 XComService_0_0_1.dxc 和 import_api_XComService_0_0_1.proto 两个文件。其中 XComService_0_0_1.dxc 为打包后的dxc 文件，可以直接安装到 xPort 目录使用，import_api_XComService_0_0_1.proto 文件是分发给其他组件，调用本组件的 IDL 文件。


3.  安装项目，命令如下：

    ```sh
    dxc install -x  ../xport
    ```

    其中 -x 标识 xPort 所在的目录。执行完成后，就可以在 xPort 的配置文件中，加载组件。