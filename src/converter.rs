use crate::x_com::{
    import_api::x_com_service_0_0_1,
    source_api::{ServiceInfo, ServiceInfos},
};

pub fn local_service_infos_to_remote(
    local_services: ServiceInfos,
) -> Vec<x_com_service_0_0_1::ServiceInfo> {
    let mut remote_services = Vec::with_capacity(local_services.service_infos.len());
    for local_service in local_services.service_infos {
        let remote_service_info = local_service_info_to_remote(local_service);
        remote_services.push(remote_service_info);
    }
    remote_services
}

pub fn local_service_info_to_remote(service_info: ServiceInfo) -> x_com_service_0_0_1::ServiceInfo {
    let mut remote_service_info = x_com_service_0_0_1::ServiceInfo::default();
    remote_service_info.dxc_name = service_info.dxc_name;
    remote_service_info.dxc_version = service_info.dxc_version;
    remote_service_info.service_name = service_info.service_name;
    remote_service_info.service_id = service_info.service_id;
    remote_service_info.online_time = service_info.online_time;

    remote_service_info.md5 = service_info.md5;
    remote_service_info
}

pub fn remote_service_infos_to_devkit(
    remote_services: Vec<x_com_service_0_0_1::ServiceInfo>,
) -> xComLib::ServiceInfos {
    let mut devkit_services = xComLib::ServiceInfos::default();
    for remote_service in remote_services {
        let mut service_info = xComLib::ServiceInfo::default();
        service_info.dxc_name = remote_service.dxc_name;
        service_info.dxc_version = remote_service.dxc_version;
        service_info.service_name = remote_service.service_name;
        service_info.service_id = remote_service.service_id;
        service_info.online_time = remote_service.online_time;
        service_info.md5 = remote_service.md5;
        devkit_services.service_infos.push(service_info);
    }

    devkit_services
}

pub fn local_service_infos_to_devkit(remote_services: Vec<ServiceInfo>) -> xComLib::ServiceInfos {
    let mut devkit_services = xComLib::ServiceInfos::default();
    for local_service in remote_services {
        let mut service_info = local_service_info_to_devkit(local_service);
        devkit_services.service_infos.push(service_info);
    }

    devkit_services
}

pub fn local_service_info_to_devkit(service_info: ServiceInfo) -> xComLib::ServiceInfo {
    let mut devkit_service_info = xComLib::ServiceInfo::default();
    devkit_service_info.dxc_name = service_info.dxc_name;
    devkit_service_info.dxc_version = service_info.dxc_version;
    devkit_service_info.service_name = service_info.service_name;
    devkit_service_info.service_id = service_info.service_id;
    devkit_service_info.md5 = service_info.md5;
    devkit_service_info.online_time = service_info.online_time;
    devkit_service_info
}

pub fn devkit_service_infos_to_local(devkit_services: xComLib::ServiceInfos) -> ServiceInfos {
    let mut service_infos = ServiceInfos::default();
    for devkit_service in devkit_services.service_infos {
        let mut service_info = ServiceInfo::default();
        service_info.dxc_name = devkit_service.dxc_name;
        service_info.dxc_version = devkit_service.dxc_version;
        service_info.service_name = devkit_service.service_name;
        service_info.service_id = devkit_service.service_id;
        service_info.md5 = devkit_service.md5;
        service_info.online_time = devkit_service.online_time;
        service_infos.service_infos.push(service_info);
    }

    service_infos
}

pub fn devkit_service_infos_to_remote(
    devkit_services: xComLib::ServiceInfos,
) -> Vec<x_com_service_0_0_1::ServiceInfo> {
    let mut service_infos = Vec::with_capacity(devkit_services.service_infos.len());

    for devkit_service in devkit_services.service_infos {
        let mut service_info = x_com_service_0_0_1::ServiceInfo::default();
        service_info.dxc_name = devkit_service.dxc_name;
        service_info.dxc_version = devkit_service.dxc_version;
        service_info.service_name = devkit_service.service_name;
        service_info.service_id = devkit_service.service_id;

        service_info.md5 = devkit_service.md5;

        service_info.online_time = devkit_service.online_time;
        service_infos.push(service_info);
    }

    service_infos
}
