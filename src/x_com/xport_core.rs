
use std::mem::MaybeUninit;
use xComLib::logger::error;
use crate::service::XComService;
use core::slice;
use xComLib::x_core;
use xComLib::x_core::xrpc;
use xComLib::x_core::parse_request_param;
use xComLib::x_core::response_msg;
use xComLib::x_core::response_empty_msg;
use xComLib::CodedInputStream;
use futures::executor::block_on;
use xComLib::x_core::gen_id;
use xComLib::x_core::set_request_id;


pub static mut SERVICE: MaybeUninit<Box<XComService>> = MaybeUninit::uninit();

#[no_mangle]
pub extern "C" fn init(service_id: i64, config: *const u8, config_len: u32, log_level: i32) {
    // 初始化日志
    block_on(async {
      let request_id = gen_id();
      set_request_id(request_id);
    let config_str = unsafe {
        let buffer =
        slice::from_raw_parts(config as *mut u8, config_len as usize);
        std::str::from_utf8_unchecked(buffer)
    };
    x_core::init_app(service_id, "XComService", &config_str, log_level);
    // 加载服务
    let service_ins = Box::new(XComService::new());
    unsafe {
      SERVICE.as_mut_ptr().write(service_ins);
      SERVICE.assume_init_mut().on_init().await;
    }
    set_request_id(0);
  });
}

#[no_mangle]
pub extern "C" fn finalize() {
  block_on(async {
    let request_id = gen_id();
    set_request_id(request_id);
    unsafe {
      SERVICE.assume_init_ref().on_finalize().await;
    }
    set_request_id(0);
  });
}
#[no_mangle]
pub extern "C" fn dispatch_message(channel_id: i64, request_id: i64, buffer: *const u8, buffer_len: u32) {
  let vec_buffer = unsafe { slice::from_raw_parts(buffer as *mut u8, buffer_len as usize) };
  let version = xComLib::get_version(&vec_buffer);
  if version != 1 {
     error!("只支持 版本 1 的协议解析！");
     return;
  }
  let reader = xComLib::ProtocolV1Reader::new(&vec_buffer);
  let ctx = xrpc::Context {
    sender_service_key: reader.sender(),
    channel_id,
    request_id,
  };
  let msg_body = reader.msg_body();
  let mut input_stream = CodedInputStream::from_bytes(msg_body);
  let _tag = input_stream.read_raw_tag_or_eof().unwrap();
  let message = input_stream.read_string().unwrap();
  set_request_id(request_id);
  match message.as_str() {
    "BuildChannel" => { 
        let param = parse_request_param(&mut input_stream);
        x_core::spawn(async move {
          let service = unsafe { SERVICE.assume_init_ref() };
          let result = service.build_channel(&ctx, param).await;
          response_empty_msg("BuildChannelRsp", request_id, &result);
        });
    }
    "LoadService" => { 
        let param = parse_request_param(&mut input_stream);
        x_core::spawn(async move {
          let service = unsafe { SERVICE.assume_init_ref() };
          let result = service.load_service(&ctx, param).await;
          response_empty_msg("LoadServiceRsp", request_id, &result);
        });
    }
    "UnloadService" => { 
        let param = parse_request_param(&mut input_stream);
        x_core::spawn(async move {
          let service = unsafe { SERVICE.assume_init_ref() };
          let result = service.unload_service(&ctx, param).await;
          response_empty_msg("UnloadServiceRsp", request_id, &result);
        });
    }
    "GetServiceList" => { 
                x_core::spawn(async move {
          let service = unsafe { SERVICE.assume_init_ref() };
          let result = service.get_service_list(&ctx).await;
          response_msg("GetServiceListRsp", request_id, &result);
        });
    }
    "ChannelConnected" => { 
        let param = parse_request_param(&mut input_stream);
        x_core::spawn(async move {
          let service = unsafe { SERVICE.assume_init_ref() };
          let result = service.channel_connected(&ctx, param).await;
          response_empty_msg("ChannelConnectedRsp", request_id, &result);
        });
    }
    "ChannelDisconnected" => { 
        let param = parse_request_param(&mut input_stream);
        x_core::spawn(async move {
          let service = unsafe { SERVICE.assume_init_ref() };
          let result = service.channel_disconnected(&ctx, param).await;
          response_empty_msg("ChannelDisconnectedRsp", request_id, &result);
        });
    }
    "SyncXrpcToRemote" => { 
        let param = parse_request_param(&mut input_stream);
        x_core::spawn(async move {
          let service = unsafe { SERVICE.assume_init_ref() };
          let result = service.sync_xrpc_to_remote(&ctx, param).await;
          response_msg("SyncXrpcToRemoteRsp", request_id, &result);
        });
    }
    "LocalServiceOn" => { 
        let param = parse_request_param(&mut input_stream);
        x_core::spawn(async move {
          let service = unsafe { SERVICE.assume_init_ref() };
          let result = service.local_service_on(&ctx, param).await;
          response_empty_msg("LocalServiceOnRsp", request_id, &result);
        });
    }
    "LocalServiceOff" => { 
        let param = parse_request_param(&mut input_stream);
        x_core::spawn(async move {
          let service = unsafe { SERVICE.assume_init_ref() };
          let result = service.local_service_off(&ctx, param).await;
          response_empty_msg("LocalServiceOffRsp", request_id, &result);
        });
    }
    "RemoteServiceOn" => { 
        let param = parse_request_param(&mut input_stream);
        x_core::spawn(async move {
          let service = unsafe { SERVICE.assume_init_ref() };
          let result = service.remote_service_on(&ctx, param).await;
          response_empty_msg("RemoteServiceOnRsp", request_id, &result);
        });
    }
    "RemoteServiceOff" => { 
        let param = parse_request_param(&mut input_stream);
        x_core::spawn(async move {
          let service = unsafe { SERVICE.assume_init_ref() };
          let result = service.remote_service_off(&ctx, param).await;
          response_empty_msg("RemoteServiceOffRsp", request_id, &result);
        });
    }
    _ => {}
  }
  set_request_id(0);
}
