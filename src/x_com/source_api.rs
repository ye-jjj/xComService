use xComLib::{CodedInputStream, CodedOutputStream, RequestMessage, Status};

pub struct UnloadServiceRequest {
    pub dxc_name: String,
    pub dxc_version: String,
    pub service_name: String,
    pub(crate) cached_size: xComLib::rt::CachedSize,
}

pub struct ServiceInfo {
    pub dxc_name: String,
    pub dxc_version: String,
    pub service_name: String,
    pub md5: String,
    pub service_id: i64,
    pub online_time: i64,
    pub(crate) cached_size: xComLib::rt::CachedSize,
}

pub struct BuildChannelRequest {
    pub node_ip: String,
    pub port: i32,
    pub(crate) cached_size: xComLib::rt::CachedSize,
}

pub struct ChannelEvent {
    pub channel_ip: String,
    pub channel_id: i64,
    pub is_active: bool,
    pub(crate) cached_size: xComLib::rt::CachedSize,
}

pub struct LoadServiceRequest {
    pub dxc_name: String,
    pub dxc_version: String,
    pub service_name: String,
    pub only_build_ipc: bool,
    pub config: String,
    pub(crate) cached_size: xComLib::rt::CachedSize,
}

pub struct ServiceInfos {
    pub service_infos: Vec<ServiceInfo>,
    pub(crate) cached_size: xComLib::rt::CachedSize,
}

pub struct NodeInfo {
    pub xrpc_port: i32,
    pub service_infos: Vec<ServiceInfo>,
    pub(crate) cached_size: xComLib::rt::CachedSize,
}

impl std::fmt::Debug for ServiceInfos {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ServiceInfos")
            .field("service_infos", &self.service_infos)
            .finish()
    }
}

impl Default for ServiceInfos {
    fn default() -> Self {
        ServiceInfos {
            service_infos: Vec::new(),
            cached_size: xComLib::rt::CachedSize::new(),
        }
    }
}

impl RequestMessage for ServiceInfos {
    fn compute_size(&self) -> u64 {
        let mut my_size = 0;
        for value in &self.service_infos {
            let len = value.compute_size();
            my_size += 1 + xComLib::rt::compute_raw_varint64_size(len) + len;
        }
        self.cached_size.set(my_size as u32);
        my_size
    }

    fn serial_with_output_stream(
        &self,
        os: &mut xComLib::CodedOutputStream<'_>,
    ) -> Result<(), Status> {
        for v in &self.service_infos {
            os.write_tag(1, xComLib::rt::WireType::LengthDelimited)
                .unwrap();
            os.write_raw_varint32(v.cached_size.get() as u32).unwrap();
            v.serial_with_output_stream(os).unwrap();
        }
        Ok(())
    }

    fn parse_from_input_stream(
        &mut self,
        is: &mut xComLib::CodedInputStream<'_>,
    ) -> Result<(), Status> {
        while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
            match tag {
                10 => {
                    let len = is.read_raw_varint64();
                    let len = len.unwrap();
                    let old_limit = is.push_limit(len);
                    let old_limit = old_limit.unwrap();
                    let mut value = ServiceInfo::default();
                    value.parse_from_input_stream(is)?;
                    self.service_infos.push(value);
                    is.pop_limit(old_limit);
                }
                _ => {}
            }
        }
        Ok(())
    }
}

impl std::fmt::Debug for BuildChannelRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("BuildChannelRequest")
            .field("node_ip", &self.node_ip)
            .field("port", &self.port)
            .finish()
    }
}

impl Default for BuildChannelRequest {
    fn default() -> Self {
        BuildChannelRequest {
            node_ip: String::default(),
            port: 0,
            cached_size: xComLib::rt::CachedSize::new(),
        }
    }
}

impl RequestMessage for BuildChannelRequest {
    fn compute_size(&self) -> u64 {
        let mut my_size = 0;
        if !self.node_ip.is_empty() {
            my_size += xComLib::rt::string_size(1, &self.node_ip);
        }
        if self.port != 0 {
            my_size += xComLib::rt::int32_size(2, self.port);
        }
        self.cached_size.set(my_size as u32);
        my_size
    }

    fn serial_with_output_stream(
        &self,
        os: &mut xComLib::CodedOutputStream<'_>,
    ) -> Result<(), Status> {
        if !self.node_ip.is_empty() {
            os.write_string(1, &self.node_ip).unwrap();
        }
        if self.port != 0 {
            os.write_int32(2, self.port).unwrap();
        }
        Ok(())
    }

    fn parse_from_input_stream(
        &mut self,
        is: &mut xComLib::CodedInputStream<'_>,
    ) -> Result<(), Status> {
        while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
            match tag {
                10 => {
                    self.node_ip = is.read_string().unwrap();
                }
                16 => {
                    self.port = is.read_int32().unwrap();
                }
                _ => {}
            }
        }
        Ok(())
    }
}

impl std::fmt::Debug for ServiceInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ServiceInfo")
            .field("dxc_name", &self.dxc_name)
            .field("dxc_version", &self.dxc_version)
            .field("service_name", &self.service_name)
            .field("md5", &self.md5)
            .field("service_id", &self.service_id)
            .field("online_time", &self.online_time)
            .finish()
    }
}

impl Default for ServiceInfo {
    fn default() -> Self {
        ServiceInfo {
            dxc_name: String::default(),
            dxc_version: String::default(),
            service_name: String::default(),
            md5: String::default(),
            service_id: 0,
            online_time: 0,
            cached_size: xComLib::rt::CachedSize::new(),
        }
    }
}

impl RequestMessage for ServiceInfo {
    fn compute_size(&self) -> u64 {
        let mut my_size = 0;
        if !self.dxc_name.is_empty() {
            my_size += xComLib::rt::string_size(1, &self.dxc_name);
        }
        if !self.dxc_version.is_empty() {
            my_size += xComLib::rt::string_size(2, &self.dxc_version);
        }
        if !self.service_name.is_empty() {
            my_size += xComLib::rt::string_size(3, &self.service_name);
        }
        if !self.md5.is_empty() {
            my_size += xComLib::rt::string_size(4, &self.md5);
        }
        if self.service_id != 0 {
            my_size += xComLib::rt::int64_size(5, self.service_id);
        }
        if self.online_time != 0 {
            my_size += xComLib::rt::int64_size(6, self.online_time);
        }
        self.cached_size.set(my_size as u32);
        my_size
    }

    fn serial_with_output_stream(
        &self,
        os: &mut xComLib::CodedOutputStream<'_>,
    ) -> Result<(), Status> {
        if !self.dxc_name.is_empty() {
            os.write_string(1, &self.dxc_name).unwrap();
        }
        if !self.dxc_version.is_empty() {
            os.write_string(2, &self.dxc_version).unwrap();
        }
        if !self.service_name.is_empty() {
            os.write_string(3, &self.service_name).unwrap();
        }
        if !self.md5.is_empty() {
            os.write_string(4, &self.md5).unwrap();
        }
        if self.service_id != 0 {
            os.write_int64(5, self.service_id).unwrap();
        }
        if self.online_time != 0 {
            os.write_int64(6, self.online_time).unwrap();
        }
        Ok(())
    }

    fn parse_from_input_stream(
        &mut self,
        is: &mut xComLib::CodedInputStream<'_>,
    ) -> Result<(), Status> {
        while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
            match tag {
                10 => {
                    self.dxc_name = is.read_string().unwrap();
                }
                18 => {
                    self.dxc_version = is.read_string().unwrap();
                }
                26 => {
                    self.service_name = is.read_string().unwrap();
                }
                34 => {
                    self.md5 = is.read_string().unwrap();
                }
                40 => {
                    self.service_id = is.read_int64().unwrap();
                }
                48 => {
                    self.online_time = is.read_int64().unwrap();
                }
                _ => {}
            }
        }
        Ok(())
    }
}

impl std::fmt::Debug for ChannelEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ChannelEvent")
            .field("channel_ip", &self.channel_ip)
            .field("channel_id", &self.channel_id)
            .field("is_active", &self.is_active)
            .finish()
    }
}

impl Default for ChannelEvent {
    fn default() -> Self {
        ChannelEvent {
            channel_ip: String::default(),
            channel_id: 0,
            is_active: false,
            cached_size: xComLib::rt::CachedSize::new(),
        }
    }
}

impl RequestMessage for ChannelEvent {
    fn compute_size(&self) -> u64 {
        let mut my_size = 0;
        if !self.channel_ip.is_empty() {
            my_size += xComLib::rt::string_size(1, &self.channel_ip);
        }
        if self.channel_id != 0 {
            my_size += xComLib::rt::int64_size(2, self.channel_id);
        }
        if self.is_active != false {
            my_size += 2;
        }
        self.cached_size.set(my_size as u32);
        my_size
    }

    fn serial_with_output_stream(
        &self,
        os: &mut xComLib::CodedOutputStream<'_>,
    ) -> Result<(), Status> {
        if !self.channel_ip.is_empty() {
            os.write_string(1, &self.channel_ip).unwrap();
        }
        if self.channel_id != 0 {
            os.write_int64(2, self.channel_id).unwrap();
        }
        if self.is_active != false {
            os.write_bool(3, self.is_active).unwrap();
        }
        Ok(())
    }

    fn parse_from_input_stream(
        &mut self,
        is: &mut xComLib::CodedInputStream<'_>,
    ) -> Result<(), Status> {
        while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
            match tag {
                10 => {
                    self.channel_ip = is.read_string().unwrap();
                }
                16 => {
                    self.channel_id = is.read_int64().unwrap();
                }
                24 => {
                    self.is_active = is.read_bool().unwrap();
                }
                _ => {}
            }
        }
        Ok(())
    }
}

impl std::fmt::Debug for LoadServiceRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("LoadServiceRequest")
            .field("dxc_name", &self.dxc_name)
            .field("dxc_version", &self.dxc_version)
            .field("service_name", &self.service_name)
            .field("only_build_ipc", &self.only_build_ipc)
            .field("config", &self.config)
            .finish()
    }
}

impl Default for LoadServiceRequest {
    fn default() -> Self {
        LoadServiceRequest {
            dxc_name: String::default(),
            dxc_version: String::default(),
            service_name: String::default(),
            only_build_ipc: false,
            config: String::default(),
            cached_size: xComLib::rt::CachedSize::new(),
        }
    }
}

impl RequestMessage for LoadServiceRequest {
    fn compute_size(&self) -> u64 {
        let mut my_size = 0;
        if !self.dxc_name.is_empty() {
            my_size += xComLib::rt::string_size(1, &self.dxc_name);
        }
        if !self.dxc_version.is_empty() {
            my_size += xComLib::rt::string_size(2, &self.dxc_version);
        }
        if !self.service_name.is_empty() {
            my_size += xComLib::rt::string_size(3, &self.service_name);
        }
        if self.only_build_ipc != false {
            my_size += 2;
        }
        if !self.config.is_empty() {
            my_size += xComLib::rt::string_size(5, &self.config);
        }
        self.cached_size.set(my_size as u32);
        my_size
    }

    fn serial_with_output_stream(
        &self,
        os: &mut xComLib::CodedOutputStream<'_>,
    ) -> Result<(), Status> {
        if !self.dxc_name.is_empty() {
            os.write_string(1, &self.dxc_name).unwrap();
        }
        if !self.dxc_version.is_empty() {
            os.write_string(2, &self.dxc_version).unwrap();
        }
        if !self.service_name.is_empty() {
            os.write_string(3, &self.service_name).unwrap();
        }
        if self.only_build_ipc != false {
            os.write_bool(4, self.only_build_ipc).unwrap();
        }
        if !self.config.is_empty() {
            os.write_string(5, &self.config).unwrap();
        }
        Ok(())
    }

    fn parse_from_input_stream(
        &mut self,
        is: &mut xComLib::CodedInputStream<'_>,
    ) -> Result<(), Status> {
        while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
            match tag {
                10 => {
                    self.dxc_name = is.read_string().unwrap();
                }
                18 => {
                    self.dxc_version = is.read_string().unwrap();
                }
                26 => {
                    self.service_name = is.read_string().unwrap();
                }
                32 => {
                    self.only_build_ipc = is.read_bool().unwrap();
                }
                42 => {
                    self.config = is.read_string().unwrap();
                }
                _ => {}
            }
        }
        Ok(())
    }
}

impl std::fmt::Debug for NodeInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("NodeInfo")
            .field("xrpc_port", &self.xrpc_port)
            .field("service_infos", &self.service_infos)
            .finish()
    }
}

impl Default for NodeInfo {
    fn default() -> Self {
        NodeInfo {
            xrpc_port: 0,
            service_infos: Vec::new(),
            cached_size: xComLib::rt::CachedSize::new(),
        }
    }
}

impl RequestMessage for NodeInfo {
    fn compute_size(&self) -> u64 {
        let mut my_size = 0;
        if self.xrpc_port != 0 {
            my_size += xComLib::rt::int32_size(1, self.xrpc_port);
        }
        for value in &self.service_infos {
            let len = value.compute_size();
            my_size += 1 + xComLib::rt::compute_raw_varint64_size(len) + len;
        }
        self.cached_size.set(my_size as u32);
        my_size
    }

    fn serial_with_output_stream(
        &self,
        os: &mut xComLib::CodedOutputStream<'_>,
    ) -> Result<(), Status> {
        if self.xrpc_port != 0 {
            os.write_int32(1, self.xrpc_port).unwrap();
        }
        for v in &self.service_infos {
            os.write_tag(2, xComLib::rt::WireType::LengthDelimited)
                .unwrap();
            os.write_raw_varint32(v.cached_size.get() as u32).unwrap();
            v.serial_with_output_stream(os).unwrap();
        }
        Ok(())
    }

    fn parse_from_input_stream(
        &mut self,
        is: &mut xComLib::CodedInputStream<'_>,
    ) -> Result<(), Status> {
        while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
            match tag {
                8 => {
                    self.xrpc_port = is.read_int32().unwrap();
                }
                18 => {
                    let len = is.read_raw_varint64();
                    let len = len.unwrap();
                    let old_limit = is.push_limit(len);
                    let old_limit = old_limit.unwrap();
                    let mut value = ServiceInfo::default();
                    value.parse_from_input_stream(is)?;
                    self.service_infos.push(value);
                    is.pop_limit(old_limit);
                }
                _ => {}
            }
        }
        Ok(())
    }
}

impl std::fmt::Debug for UnloadServiceRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("UnloadServiceRequest")
            .field("dxc_name", &self.dxc_name)
            .field("dxc_version", &self.dxc_version)
            .field("service_name", &self.service_name)
            .finish()
    }
}

impl Default for UnloadServiceRequest {
    fn default() -> Self {
        UnloadServiceRequest {
            dxc_name: String::default(),
            dxc_version: String::default(),
            service_name: String::default(),
            cached_size: xComLib::rt::CachedSize::new(),
        }
    }
}

impl RequestMessage for UnloadServiceRequest {
    fn compute_size(&self) -> u64 {
        let mut my_size = 0;
        if !self.dxc_name.is_empty() {
            my_size += xComLib::rt::string_size(1, &self.dxc_name);
        }
        if !self.dxc_version.is_empty() {
            my_size += xComLib::rt::string_size(2, &self.dxc_version);
        }
        if !self.service_name.is_empty() {
            my_size += xComLib::rt::string_size(3, &self.service_name);
        }
        self.cached_size.set(my_size as u32);
        my_size
    }

    fn serial_with_output_stream(
        &self,
        os: &mut xComLib::CodedOutputStream<'_>,
    ) -> Result<(), Status> {
        if !self.dxc_name.is_empty() {
            os.write_string(1, &self.dxc_name).unwrap();
        }
        if !self.dxc_version.is_empty() {
            os.write_string(2, &self.dxc_version).unwrap();
        }
        if !self.service_name.is_empty() {
            os.write_string(3, &self.service_name).unwrap();
        }
        Ok(())
    }

    fn parse_from_input_stream(
        &mut self,
        is: &mut xComLib::CodedInputStream<'_>,
    ) -> Result<(), Status> {
        while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
            match tag {
                10 => {
                    self.dxc_name = is.read_string().unwrap();
                }
                18 => {
                    self.dxc_version = is.read_string().unwrap();
                }
                26 => {
                    self.service_name = is.read_string().unwrap();
                }
                _ => {}
            }
        }
        Ok(())
    }
}
