pub mod x_com_service_0_0_1 {
    use xComLib::x_api;
    use xComLib::x_core::{
        add_request_handler, build_request_empty_future, build_request_future,
        parse_empty_response_and_wake, parse_response_and_wake, RequestEmptyFuture, RequestFuture,
    };
    use xComLib::ServiceKey;
    use xComLib::{
        x_core::{serial_empty_request, serial_request},
        RequestMessage, Status,
    };

    pub struct NodeInfo {
        pub xrpc_port: i32,
        pub service_infos: Vec<ServiceInfo>,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    pub struct UnloadServiceRequest {
        pub dxc_name: String,
        pub dxc_version: String,
        pub service_name: String,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    pub struct BuildChannelRequest {
        pub node_ip: String,
        pub port: i32,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    pub struct ServiceInfos {
        pub service_infos: Vec<ServiceInfo>,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    pub struct ChannelEvent {
        pub channel_ip: String,
        pub channel_id: i64,
        pub is_active: bool,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    pub struct LoadServiceRequest {
        pub dxc_name: String,
        pub dxc_version: String,
        pub service_name: String,
        pub only_build_ipc: bool,
        pub config: String,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    pub struct ServiceInfo {
        pub dxc_name: String,
        pub dxc_version: String,
        pub service_name: String,
        pub md5: String,
        pub service_id: i64,
        pub online_time: i64,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    impl std::fmt::Debug for ServiceInfos {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("ServiceInfos")
                .field("service_infos", &self.service_infos)
                .finish()
        }
    }

    impl Default for ServiceInfos {
        fn default() -> Self {
            ServiceInfos {
                service_infos: Vec::new(),
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for ServiceInfos {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            for value in &self.service_infos {
                let len = value.compute_size();
                my_size += 1 + xComLib::rt::compute_raw_varint64_size(len) + len;
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            for v in &self.service_infos {
                os.write_tag(1, xComLib::rt::WireType::LengthDelimited)
                    .unwrap();
                os.write_raw_varint32(v.cached_size.get() as u32).unwrap();
                v.serial_with_output_stream(os).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    10 => {
                        let len = is.read_raw_varint64();
                        let len = len.unwrap();
                        let old_limit = is.push_limit(len);
                        let old_limit = old_limit.unwrap();
                        let mut value = ServiceInfo::default();
                        value.parse_from_input_stream(is)?;
                        self.service_infos.push(value);
                        is.pop_limit(old_limit);
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl std::fmt::Debug for BuildChannelRequest {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("BuildChannelRequest")
                .field("node_ip", &self.node_ip)
                .field("port", &self.port)
                .finish()
        }
    }

    impl Default for BuildChannelRequest {
        fn default() -> Self {
            BuildChannelRequest {
                node_ip: String::default(),
                port: 0,
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for BuildChannelRequest {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            if !self.node_ip.is_empty() {
                my_size += xComLib::rt::string_size(1, &self.node_ip);
            }
            if self.port != 0 {
                my_size += xComLib::rt::int32_size(2, self.port);
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            if !self.node_ip.is_empty() {
                os.write_string(1, &self.node_ip).unwrap();
            }
            if self.port != 0 {
                os.write_int32(2, self.port).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    10 => {
                        self.node_ip = is.read_string().unwrap();
                    }
                    16 => {
                        self.port = is.read_int32().unwrap();
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl std::fmt::Debug for ServiceInfo {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("ServiceInfo")
                .field("dxc_name", &self.dxc_name)
                .field("dxc_version", &self.dxc_version)
                .field("service_name", &self.service_name)
                .field("md5", &self.md5)
                .field("service_id", &self.service_id)
                .field("online_time", &self.online_time)
                .finish()
        }
    }

    impl Default for ServiceInfo {
        fn default() -> Self {
            ServiceInfo {
                dxc_name: String::default(),
                dxc_version: String::default(),
                service_name: String::default(),
                md5: String::default(),
                service_id: 0,
                online_time: 0,
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for ServiceInfo {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            if !self.dxc_name.is_empty() {
                my_size += xComLib::rt::string_size(1, &self.dxc_name);
            }
            if !self.dxc_version.is_empty() {
                my_size += xComLib::rt::string_size(2, &self.dxc_version);
            }
            if !self.service_name.is_empty() {
                my_size += xComLib::rt::string_size(3, &self.service_name);
            }
            if !self.md5.is_empty() {
                my_size += xComLib::rt::string_size(4, &self.md5);
            }
            if self.service_id != 0 {
                my_size += xComLib::rt::int64_size(5, self.service_id);
            }
            if self.online_time != 0 {
                my_size += xComLib::rt::int64_size(6, self.online_time);
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            if !self.dxc_name.is_empty() {
                os.write_string(1, &self.dxc_name).unwrap();
            }
            if !self.dxc_version.is_empty() {
                os.write_string(2, &self.dxc_version).unwrap();
            }
            if !self.service_name.is_empty() {
                os.write_string(3, &self.service_name).unwrap();
            }
            if !self.md5.is_empty() {
                os.write_string(4, &self.md5).unwrap();
            }
            if self.service_id != 0 {
                os.write_int64(5, self.service_id).unwrap();
            }
            if self.online_time != 0 {
                os.write_int64(6, self.online_time).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    10 => {
                        self.dxc_name = is.read_string().unwrap();
                    }
                    18 => {
                        self.dxc_version = is.read_string().unwrap();
                    }
                    26 => {
                        self.service_name = is.read_string().unwrap();
                    }
                    34 => {
                        self.md5 = is.read_string().unwrap();
                    }
                    40 => {
                        self.service_id = is.read_int64().unwrap();
                    }
                    48 => {
                        self.online_time = is.read_int64().unwrap();
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl std::fmt::Debug for ChannelEvent {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("ChannelEvent")
                .field("channel_ip", &self.channel_ip)
                .field("channel_id", &self.channel_id)
                .field("is_active", &self.is_active)
                .finish()
        }
    }

    impl Default for ChannelEvent {
        fn default() -> Self {
            ChannelEvent {
                channel_ip: String::default(),
                channel_id: 0,
                is_active: false,
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for ChannelEvent {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            if !self.channel_ip.is_empty() {
                my_size += xComLib::rt::string_size(1, &self.channel_ip);
            }
            if self.channel_id != 0 {
                my_size += xComLib::rt::int64_size(2, self.channel_id);
            }
            if self.is_active != false {
                my_size += 2;
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            if !self.channel_ip.is_empty() {
                os.write_string(1, &self.channel_ip).unwrap();
            }
            if self.channel_id != 0 {
                os.write_int64(2, self.channel_id).unwrap();
            }
            if self.is_active != false {
                os.write_bool(3, self.is_active).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    10 => {
                        self.channel_ip = is.read_string().unwrap();
                    }
                    16 => {
                        self.channel_id = is.read_int64().unwrap();
                    }
                    24 => {
                        self.is_active = is.read_bool().unwrap();
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl std::fmt::Debug for LoadServiceRequest {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("LoadServiceRequest")
                .field("dxc_name", &self.dxc_name)
                .field("dxc_version", &self.dxc_version)
                .field("service_name", &self.service_name)
                .field("only_build_ipc", &self.only_build_ipc)
                .field("config", &self.config)
                .finish()
        }
    }

    impl Default for LoadServiceRequest {
        fn default() -> Self {
            LoadServiceRequest {
                dxc_name: String::default(),
                dxc_version: String::default(),
                service_name: String::default(),
                only_build_ipc: false,
                config: String::default(),
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for LoadServiceRequest {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            if !self.dxc_name.is_empty() {
                my_size += xComLib::rt::string_size(1, &self.dxc_name);
            }
            if !self.dxc_version.is_empty() {
                my_size += xComLib::rt::string_size(2, &self.dxc_version);
            }
            if !self.service_name.is_empty() {
                my_size += xComLib::rt::string_size(3, &self.service_name);
            }
            if self.only_build_ipc != false {
                my_size += 2;
            }
            if !self.config.is_empty() {
                my_size += xComLib::rt::string_size(5, &self.config);
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            if !self.dxc_name.is_empty() {
                os.write_string(1, &self.dxc_name).unwrap();
            }
            if !self.dxc_version.is_empty() {
                os.write_string(2, &self.dxc_version).unwrap();
            }
            if !self.service_name.is_empty() {
                os.write_string(3, &self.service_name).unwrap();
            }
            if self.only_build_ipc != false {
                os.write_bool(4, self.only_build_ipc).unwrap();
            }
            if !self.config.is_empty() {
                os.write_string(5, &self.config).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    10 => {
                        self.dxc_name = is.read_string().unwrap();
                    }
                    18 => {
                        self.dxc_version = is.read_string().unwrap();
                    }
                    26 => {
                        self.service_name = is.read_string().unwrap();
                    }
                    32 => {
                        self.only_build_ipc = is.read_bool().unwrap();
                    }
                    42 => {
                        self.config = is.read_string().unwrap();
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl std::fmt::Debug for NodeInfo {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("NodeInfo")
                .field("xrpc_port", &self.xrpc_port)
                .field("service_infos", &self.service_infos)
                .finish()
        }
    }

    impl Default for NodeInfo {
        fn default() -> Self {
            NodeInfo {
                xrpc_port: 0,
                service_infos: Vec::new(),
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for NodeInfo {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            if self.xrpc_port != 0 {
                my_size += xComLib::rt::int32_size(1, self.xrpc_port);
            }
            for value in &self.service_infos {
                let len = value.compute_size();
                my_size += 1 + xComLib::rt::compute_raw_varint64_size(len) + len;
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            if self.xrpc_port != 0 {
                os.write_int32(1, self.xrpc_port).unwrap();
            }
            for v in &self.service_infos {
                os.write_tag(2, xComLib::rt::WireType::LengthDelimited)
                    .unwrap();
                os.write_raw_varint32(v.cached_size.get() as u32).unwrap();
                v.serial_with_output_stream(os).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    8 => {
                        self.xrpc_port = is.read_int32().unwrap();
                    }
                    18 => {
                        let len = is.read_raw_varint64();
                        let len = len.unwrap();
                        let old_limit = is.push_limit(len);
                        let old_limit = old_limit.unwrap();
                        let mut value = ServiceInfo::default();
                        value.parse_from_input_stream(is)?;
                        self.service_infos.push(value);
                        is.pop_limit(old_limit);
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl std::fmt::Debug for UnloadServiceRequest {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("UnloadServiceRequest")
                .field("dxc_name", &self.dxc_name)
                .field("dxc_version", &self.dxc_version)
                .field("service_name", &self.service_name)
                .finish()
        }
    }

    impl Default for UnloadServiceRequest {
        fn default() -> Self {
            UnloadServiceRequest {
                dxc_name: String::default(),
                dxc_version: String::default(),
                service_name: String::default(),
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for UnloadServiceRequest {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            if !self.dxc_name.is_empty() {
                my_size += xComLib::rt::string_size(1, &self.dxc_name);
            }
            if !self.dxc_version.is_empty() {
                my_size += xComLib::rt::string_size(2, &self.dxc_version);
            }
            if !self.service_name.is_empty() {
                my_size += xComLib::rt::string_size(3, &self.service_name);
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            if !self.dxc_name.is_empty() {
                os.write_string(1, &self.dxc_name).unwrap();
            }
            if !self.dxc_version.is_empty() {
                os.write_string(2, &self.dxc_version).unwrap();
            }
            if !self.service_name.is_empty() {
                os.write_string(3, &self.service_name).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    10 => {
                        self.dxc_name = is.read_string().unwrap();
                    }
                    18 => {
                        self.dxc_version = is.read_string().unwrap();
                    }
                    26 => {
                        self.service_name = is.read_string().unwrap();
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    static DXC_NAME: &str = "XComService";
    static DXC_VERSION: &str = "0.0.1";
    static DEFAULT_SERVICE_NAME: &str = "XComService";

    pub fn build_channel(param: &BuildChannelRequest) -> RequestEmptyFuture {
        build_channel_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0, param)
    }
    pub fn build_channel_with_channel_id(
        channel_id: i64,
        param: &BuildChannelRequest,
    ) -> RequestEmptyFuture {
        build_channel_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, channel_id, param)
    }
    pub fn build_channel_with_service_name(
        service_name: &str,
        param: &BuildChannelRequest,
    ) -> RequestEmptyFuture {
        build_channel_with_service_name_and_channel_id(service_name, 0, param)
    }
    pub fn build_channel_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
        param: &BuildChannelRequest,
    ) -> RequestEmptyFuture {
        let (future, request_id, is_succeed) = build_request_empty_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_request("BuildChannel", param);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_empty_response_and_wake(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
    pub fn load_service(param: &LoadServiceRequest) -> RequestEmptyFuture {
        load_service_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0, param)
    }
    pub fn load_service_with_channel_id(
        channel_id: i64,
        param: &LoadServiceRequest,
    ) -> RequestEmptyFuture {
        load_service_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, channel_id, param)
    }
    pub fn load_service_with_service_name(
        service_name: &str,
        param: &LoadServiceRequest,
    ) -> RequestEmptyFuture {
        load_service_with_service_name_and_channel_id(service_name, 0, param)
    }
    pub fn load_service_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
        param: &LoadServiceRequest,
    ) -> RequestEmptyFuture {
        let (future, request_id, is_succeed) = build_request_empty_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_request("LoadService", param);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_empty_response_and_wake(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
    pub fn unload_service(param: &UnloadServiceRequest) -> RequestEmptyFuture {
        unload_service_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0, param)
    }
    pub fn unload_service_with_channel_id(
        channel_id: i64,
        param: &UnloadServiceRequest,
    ) -> RequestEmptyFuture {
        unload_service_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, channel_id, param)
    }
    pub fn unload_service_with_service_name(
        service_name: &str,
        param: &UnloadServiceRequest,
    ) -> RequestEmptyFuture {
        unload_service_with_service_name_and_channel_id(service_name, 0, param)
    }
    pub fn unload_service_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
        param: &UnloadServiceRequest,
    ) -> RequestEmptyFuture {
        let (future, request_id, is_succeed) = build_request_empty_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_request("UnloadService", param);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_empty_response_and_wake(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
    pub fn get_service_list() -> RequestFuture<ServiceInfos> {
        get_service_list_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0)
    }
    pub fn get_service_list_with_channel_id(channel_id: i64) -> RequestFuture<ServiceInfos> {
        get_service_list_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, channel_id)
    }
    pub fn get_service_list_with_service_name(service_name: &str) -> RequestFuture<ServiceInfos> {
        get_service_list_with_service_name_and_channel_id(service_name, 0)
    }
    pub fn get_service_list_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
    ) -> RequestFuture<ServiceInfos> {
        let (future, request_id, is_succeed) = build_request_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_empty_request("GetServiceList");
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_response_and_wake::<ServiceInfos>(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
    pub fn channel_connected(param: &ChannelEvent) -> RequestEmptyFuture {
        channel_connected_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0, param)
    }
    pub fn channel_connected_with_channel_id(
        channel_id: i64,
        param: &ChannelEvent,
    ) -> RequestEmptyFuture {
        channel_connected_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, channel_id, param)
    }
    pub fn channel_connected_with_service_name(
        service_name: &str,
        param: &ChannelEvent,
    ) -> RequestEmptyFuture {
        channel_connected_with_service_name_and_channel_id(service_name, 0, param)
    }
    pub fn channel_connected_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
        param: &ChannelEvent,
    ) -> RequestEmptyFuture {
        let (future, request_id, is_succeed) = build_request_empty_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_request("ChannelConnected", param);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_empty_response_and_wake(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
    pub fn channel_disconnected(param: &ChannelEvent) -> RequestEmptyFuture {
        channel_disconnected_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0, param)
    }
    pub fn channel_disconnected_with_channel_id(
        channel_id: i64,
        param: &ChannelEvent,
    ) -> RequestEmptyFuture {
        channel_disconnected_with_service_name_and_channel_id(
            &DEFAULT_SERVICE_NAME,
            channel_id,
            param,
        )
    }
    pub fn channel_disconnected_with_service_name(
        service_name: &str,
        param: &ChannelEvent,
    ) -> RequestEmptyFuture {
        channel_disconnected_with_service_name_and_channel_id(service_name, 0, param)
    }
    pub fn channel_disconnected_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
        param: &ChannelEvent,
    ) -> RequestEmptyFuture {
        let (future, request_id, is_succeed) = build_request_empty_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_request("ChannelDisconnected", param);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_empty_response_and_wake(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
    pub fn sync_xrpc_to_remote(param: &NodeInfo) -> RequestFuture<ServiceInfos> {
        sync_xrpc_to_remote_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0, param)
    }
    pub fn sync_xrpc_to_remote_with_channel_id(
        channel_id: i64,
        param: &NodeInfo,
    ) -> RequestFuture<ServiceInfos> {
        sync_xrpc_to_remote_with_service_name_and_channel_id(
            &DEFAULT_SERVICE_NAME,
            channel_id,
            param,
        )
    }
    pub fn sync_xrpc_to_remote_with_service_name(
        service_name: &str,
        param: &NodeInfo,
    ) -> RequestFuture<ServiceInfos> {
        sync_xrpc_to_remote_with_service_name_and_channel_id(service_name, 0, param)
    }
    pub fn sync_xrpc_to_remote_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
        param: &NodeInfo,
    ) -> RequestFuture<ServiceInfos> {
        let (future, request_id, is_succeed) = build_request_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_request("SyncXrpcToRemote", param);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_response_and_wake::<ServiceInfos>(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
    pub fn local_service_on(param: &ServiceInfo) -> RequestEmptyFuture {
        local_service_on_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0, param)
    }
    pub fn local_service_on_with_channel_id(
        channel_id: i64,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        local_service_on_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, channel_id, param)
    }
    pub fn local_service_on_with_service_name(
        service_name: &str,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        local_service_on_with_service_name_and_channel_id(service_name, 0, param)
    }
    pub fn local_service_on_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        let (future, request_id, is_succeed) = build_request_empty_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_request("LocalServiceOn", param);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_empty_response_and_wake(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
    pub fn local_service_off(param: &ServiceInfo) -> RequestEmptyFuture {
        local_service_off_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0, param)
    }
    pub fn local_service_off_with_channel_id(
        channel_id: i64,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        local_service_off_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, channel_id, param)
    }
    pub fn local_service_off_with_service_name(
        service_name: &str,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        local_service_off_with_service_name_and_channel_id(service_name, 0, param)
    }
    pub fn local_service_off_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        let (future, request_id, is_succeed) = build_request_empty_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_request("LocalServiceOff", param);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_empty_response_and_wake(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
    pub fn remote_service_on(param: &ServiceInfo) -> RequestEmptyFuture {
        remote_service_on_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0, param)
    }
    pub fn remote_service_on_with_channel_id(
        channel_id: i64,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        remote_service_on_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, channel_id, param)
    }
    pub fn remote_service_on_with_service_name(
        service_name: &str,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        remote_service_on_with_service_name_and_channel_id(service_name, 0, param)
    }
    pub fn remote_service_on_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        let (future, request_id, is_succeed) = build_request_empty_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_request("RemoteServiceOn", param);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_empty_response_and_wake(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
    pub fn remote_service_off(param: &ServiceInfo) -> RequestEmptyFuture {
        remote_service_off_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0, param)
    }
    pub fn remote_service_off_with_channel_id(
        channel_id: i64,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        remote_service_off_with_service_name_and_channel_id(
            &DEFAULT_SERVICE_NAME,
            channel_id,
            param,
        )
    }
    pub fn remote_service_off_with_service_name(
        service_name: &str,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        remote_service_off_with_service_name_and_channel_id(service_name, 0, param)
    }
    pub fn remote_service_off_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
        param: &ServiceInfo,
    ) -> RequestEmptyFuture {
        let (future, request_id, is_succeed) = build_request_empty_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_request("RemoteServiceOff", param);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_empty_response_and_wake(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
}
pub mod register_service_0_1_0 {
    use xComLib::x_api;
    use xComLib::x_core::{
        add_request_handler, build_request_empty_future, build_request_future,
        parse_empty_response_and_wake, parse_response_and_wake, RequestEmptyFuture, RequestFuture,
    };
    use xComLib::ServiceKey;
    use xComLib::{
        x_core::{self, serial_empty_request, serial_request},
        CodedInputStream, CodedOutputStream, RequestMessage, Status,
    };

    pub struct HelloReply {
        pub greeting: String,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    pub struct HttpResponse {
        pub headers: std::collections::HashMap<String, String>,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    pub mod http_response {}

    #[derive(Clone, Copy, Debug)]
    pub enum HTTPMethod {
        GET = 0,
        POST = 1,
    }

    pub struct LocalServiceInfos {
        pub services: Vec<local_service_infos::LocalServiceInfo>,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    pub mod local_service_infos {
        pub struct LocalServiceInfo {
            pub dxc_name: String,
            pub dxc_version: String,
            pub service_name: String,
            pub service_id: i64,
            pub online_time: i64,
            pub(crate) cached_size: xComLib::rt::CachedSize,
        }
    }

    pub struct Hello {
        pub name: String,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    pub struct HttpRequest {
        pub method: Option<HTTPMethod>,
        pub url: String,
        pub headers: std::collections::HashMap<String, String>,
        pub body: String,
        pub(crate) cached_size: xComLib::rt::CachedSize,
    }

    pub mod http_request {}

    impl std::fmt::Debug for local_service_infos::LocalServiceInfo {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("local_service_infos::LocalServiceInfo")
                .field("dxc_name", &self.dxc_name)
                .field("dxc_version", &self.dxc_version)
                .field("service_name", &self.service_name)
                .field("service_id", &self.service_id)
                .field("online_time", &self.online_time)
                .finish()
        }
    }

    impl Default for local_service_infos::LocalServiceInfo {
        fn default() -> Self {
            local_service_infos::LocalServiceInfo {
                dxc_name: String::default(),
                dxc_version: String::default(),
                service_name: String::default(),
                service_id: 0,
                online_time: 0,
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for local_service_infos::LocalServiceInfo {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            if !self.dxc_name.is_empty() {
                my_size += xComLib::rt::string_size(1, &self.dxc_name);
            }
            if !self.dxc_version.is_empty() {
                my_size += xComLib::rt::string_size(2, &self.dxc_version);
            }
            if !self.service_name.is_empty() {
                my_size += xComLib::rt::string_size(3, &self.service_name);
            }
            if self.service_id != 0 {
                my_size += xComLib::rt::int64_size(4, self.service_id);
            }
            if self.online_time != 0 {
                my_size += xComLib::rt::int64_size(5, self.online_time);
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            if !self.dxc_name.is_empty() {
                os.write_string(1, &self.dxc_name).unwrap();
            }
            if !self.dxc_version.is_empty() {
                os.write_string(2, &self.dxc_version).unwrap();
            }
            if !self.service_name.is_empty() {
                os.write_string(3, &self.service_name).unwrap();
            }
            if self.service_id != 0 {
                os.write_int64(4, self.service_id).unwrap();
            }
            if self.online_time != 0 {
                os.write_int64(5, self.online_time).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    10 => {
                        self.dxc_name = is.read_string().unwrap();
                    }
                    18 => {
                        self.dxc_version = is.read_string().unwrap();
                    }
                    26 => {
                        self.service_name = is.read_string().unwrap();
                    }
                    32 => {
                        self.service_id = is.read_int64().unwrap();
                    }
                    40 => {
                        self.online_time = is.read_int64().unwrap();
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl std::fmt::Debug for Hello {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("Hello").field("name", &self.name).finish()
        }
    }

    impl Default for Hello {
        fn default() -> Self {
            Hello {
                name: String::default(),
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for Hello {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            if !self.name.is_empty() {
                my_size += xComLib::rt::string_size(1, &self.name);
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            if !self.name.is_empty() {
                os.write_string(1, &self.name).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    10 => {
                        self.name = is.read_string().unwrap();
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl std::fmt::Debug for HelloReply {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("HelloReply")
                .field("greeting", &self.greeting)
                .finish()
        }
    }

    impl Default for HelloReply {
        fn default() -> Self {
            HelloReply {
                greeting: String::default(),
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for HelloReply {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            if !self.greeting.is_empty() {
                my_size += xComLib::rt::string_size(1, &self.greeting);
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            if !self.greeting.is_empty() {
                os.write_string(1, &self.greeting).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    10 => {
                        self.greeting = is.read_string().unwrap();
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl std::fmt::Debug for HttpResponse {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("HttpResponse")
                .field("headers", &self.headers)
                .finish()
        }
    }

    impl Default for HttpResponse {
        fn default() -> Self {
            HttpResponse {
                headers: std::collections::HashMap::new(),
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for HttpResponse {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            for (k, v) in &self.headers {
                let mut entry_size = 0;
                entry_size += xComLib::rt::string_size(1, &k);
                entry_size += xComLib::rt::string_size(2, &v);
                my_size += 1 + xComLib::rt::compute_raw_varint64_size(entry_size) + entry_size;
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            for (k, v) in &self.headers {
                let mut entry_size = 0;
                entry_size += xComLib::rt::string_size(1, &k);
                entry_size += xComLib::rt::string_size(2, &v);
                os.write_raw_varint32(10).unwrap();
                os.write_raw_varint32(entry_size as u32).unwrap();
                os.write_string(1, &k).unwrap();
                os.write_string(2, &v).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    10 => {
                        let len = is.read_raw_varint32();
                        let len = len.unwrap();
                        let old_limit = is.push_limit(len as u64).unwrap();
                        let mut key = String::default();
                        let mut value = String::default();
                        while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                            match tag {
                                10 => {
                                    key = is.read_string().unwrap();
                                }
                                18 => {
                                    value = is.read_string().unwrap();
                                }
                                _ => xComLib::rt::skip_field_for_tag(tag, is).unwrap(),
                            }
                        }
                        is.pop_limit(old_limit);
                        self.headers.insert(key, value);
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl std::fmt::Debug for LocalServiceInfos {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("LocalServiceInfos")
                .field("services", &self.services)
                .finish()
        }
    }

    impl Default for LocalServiceInfos {
        fn default() -> Self {
            LocalServiceInfos {
                services: Vec::new(),
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for LocalServiceInfos {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            for value in &self.services {
                let len = value.compute_size();
                my_size += 1 + xComLib::rt::compute_raw_varint64_size(len) + len;
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            for v in &self.services {
                os.write_tag(1, xComLib::rt::WireType::LengthDelimited)
                    .unwrap();
                os.write_raw_varint32(v.cached_size.get() as u32).unwrap();
                v.serial_with_output_stream(os).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    10 => {
                        let len = is.read_raw_varint64();
                        let len = len.unwrap();
                        let old_limit = is.push_limit(len);
                        let old_limit = old_limit.unwrap();
                        let mut value = local_service_infos::LocalServiceInfo::default();
                        value.parse_from_input_stream(is)?;
                        self.services.push(value);
                        is.pop_limit(old_limit);
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl std::fmt::Debug for HttpRequest {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("HttpRequest")
                .field("method", &self.method)
                .field("url", &self.url)
                .field("headers", &self.headers)
                .field("body", &self.body)
                .finish()
        }
    }

    impl Default for HttpRequest {
        fn default() -> Self {
            HttpRequest {
                method: HTTPMethod::from_i32(0),
                url: String::default(),
                headers: std::collections::HashMap::new(),
                body: String::default(),
                cached_size: xComLib::rt::CachedSize::new(),
            }
        }
    }

    impl RequestMessage for HttpRequest {
        fn compute_size(&self) -> u64 {
            let mut my_size = 0;
            if let Some(value) = self.method.as_ref() {
                my_size += xComLib::rt::int32_size(1, *value as i32);
            }
            if !self.url.is_empty() {
                my_size += xComLib::rt::string_size(2, &self.url);
            }
            for (k, v) in &self.headers {
                let mut entry_size = 0;
                entry_size += xComLib::rt::string_size(1, &k);
                entry_size += xComLib::rt::string_size(2, &v);
                my_size += 1 + xComLib::rt::compute_raw_varint64_size(entry_size) + entry_size;
            }
            if !self.body.is_empty() {
                my_size += xComLib::rt::string_size(4, &self.body);
            }
            self.cached_size.set(my_size as u32);
            my_size
        }

        fn serial_with_output_stream(
            &self,
            os: &mut xComLib::CodedOutputStream<'_>,
        ) -> Result<(), Status> {
            if let Some(value) = self.method.as_ref() {
                os.write_enum(1, *value as i32).unwrap();
            }
            if !self.url.is_empty() {
                os.write_string(2, &self.url).unwrap();
            }
            for (k, v) in &self.headers {
                let mut entry_size = 0;
                entry_size += xComLib::rt::string_size(1, &k);
                entry_size += xComLib::rt::string_size(2, &v);
                os.write_raw_varint32(26).unwrap();
                os.write_raw_varint32(entry_size as u32).unwrap();
                os.write_string(1, &k).unwrap();
                os.write_string(2, &v).unwrap();
            }
            if !self.body.is_empty() {
                os.write_string(4, &self.body).unwrap();
            }
            Ok(())
        }

        fn parse_from_input_stream(
            &mut self,
            is: &mut xComLib::CodedInputStream<'_>,
        ) -> Result<(), Status> {
            while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                match tag {
                    8 => {
                        let value = HTTPMethod::from_i32(is.read_int32().unwrap());
                        self.method = value;
                    }
                    18 => {
                        self.url = is.read_string().unwrap();
                    }
                    26 => {
                        let len = is.read_raw_varint32();
                        let len = len.unwrap();
                        let old_limit = is.push_limit(len as u64).unwrap();
                        let mut key = String::default();
                        let mut value = String::default();
                        while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {
                            match tag {
                                10 => {
                                    key = is.read_string().unwrap();
                                }
                                18 => {
                                    value = is.read_string().unwrap();
                                }
                                _ => xComLib::rt::skip_field_for_tag(tag, is).unwrap(),
                            }
                        }
                        is.pop_limit(old_limit);
                        self.headers.insert(key, value);
                    }
                    34 => {
                        self.body = is.read_string().unwrap();
                    }
                    _ => {}
                }
            }
            Ok(())
        }
    }

    impl HTTPMethod {
        fn from_i32(value: i32) -> Option<HTTPMethod> {
            match value {
                0 => Some(Self::GET),
                1 => Some(Self::POST),
                _ => None,
            }
        }
    }

    static DXC_NAME: &str = "RegisterService";
    static DXC_VERSION: &str = "0.1.0";
    static DEFAULT_SERVICE_NAME: &str = "RegisterService";

    pub fn say_hello(param: &Hello) -> RequestFuture<HelloReply> {
        say_hello_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0, param)
    }
    pub fn say_hello_with_channel_id(channel_id: i64, param: &Hello) -> RequestFuture<HelloReply> {
        say_hello_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, channel_id, param)
    }
    pub fn say_hello_with_service_name(
        service_name: &str,
        param: &Hello,
    ) -> RequestFuture<HelloReply> {
        say_hello_with_service_name_and_channel_id(service_name, 0, param)
    }
    pub fn say_hello_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
        param: &Hello,
    ) -> RequestFuture<HelloReply> {
        let (future, request_id, is_succeed) = build_request_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_request("SayHello", param);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_response_and_wake::<HelloReply>(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
    pub fn empty_param_and_reply() -> RequestEmptyFuture {
        empty_param_and_reply_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, 0)
    }
    pub fn empty_param_and_reply_with_channel_id(channel_id: i64) -> RequestEmptyFuture {
        empty_param_and_reply_with_service_name_and_channel_id(&DEFAULT_SERVICE_NAME, channel_id)
    }
    pub fn empty_param_and_reply_with_service_name(service_name: &str) -> RequestEmptyFuture {
        empty_param_and_reply_with_service_name_and_channel_id(service_name, 0)
    }
    pub fn empty_param_and_reply_with_service_name_and_channel_id(
        service_name: &str,
        channel_id: i64,
    ) -> RequestEmptyFuture {
        let (future, request_id, is_succeed) = build_request_empty_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_empty_request("EmptyParamAndReply");
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_empty_response_and_wake(&clone_shared_state, buffer);
            }),
        );
        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = DXC_NAME.into();
        recveiver.dxc_version = DXC_VERSION.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, channel_id, buffer);
        future
    }
}

pub mod ipc_service {
    use xComLib::{
        x_api,
        x_core::{
            add_request_handler, build_request_empty_future, parse_empty_response_and_wake,
            serial_empty_request, RequestEmptyFuture,
        },
        ServiceKey,
    };

    fn call_ipc(
        dxc_name: &str,
        dxc_version: &str,
        service_name: &str,
        method: &str,
    ) -> RequestEmptyFuture {
        //
        let (future, request_id, is_succeed) = build_request_empty_future();
        if !is_succeed {
            return future;
        }
        let buffer = serial_empty_request(method);
        let clone_shared_state = future.shared_state.clone();
        add_request_handler(
            request_id,
            Box::new(move |buffer| {
                parse_empty_response_and_wake(&clone_shared_state, buffer);
            }),
        );

        let mut recveiver = ServiceKey::default();
        recveiver.dxc_name = dxc_name.into();
        recveiver.dxc_version = dxc_version.into();
        recveiver.service_name = service_name.into();
        x_api::xport::send_message(request_id, recveiver, 0, buffer);
        future
    }

    //
    pub fn on_init(dxc_name: &str, dxc_version: &str, service_name: &str) -> RequestEmptyFuture {
      call_ipc(dxc_name, dxc_version, service_name, "on_init")
    }
    //

    pub fn on_finalize(dxc_name: &str, dxc_version: &str, service_name: &str) -> RequestEmptyFuture {
      call_ipc(dxc_name, dxc_version, service_name, "on_finalize")
  }
}
