use crate::converter::{
    devkit_service_infos_to_local, devkit_service_infos_to_remote, local_service_info_to_devkit,
    local_service_info_to_remote, local_service_infos_to_devkit, local_service_infos_to_remote,
    remote_service_infos_to_devkit,
};
use crate::x_com::{import_api::x_com_service_0_0_1, source_api::*};

use xComLib::x_api::xport::{
    add_channel_id_to_remote_services, get_all_conn_id, get_channel_id_by_conn_id, get_xrpc_port,
    remove_remote_services_all_channel_id, set_channel_xrpc_port, sleep,
};
use xComLib::x_core::xrpc::Context;
use xComLib::{
    conn_id_to_addr,
    logger::error,
    logger::info,
    make_node_id,
    x_api::{
        self,
        xport::{build_channel, get_all_local_service},
    },
};
use xComLib::{x_core, CachedSize};

pub struct XComService {}

impl XComService {
    //
    pub fn new() -> XComService {
        XComService {}
    }
    pub async fn on_init(&self) {
        info!("初始化...")
    }

    pub async fn on_finalize(&self) {}

    async fn get_service_list_with_filter(
        &self,
        filter_system: bool,
    ) -> x_core::Result<ServiceInfos> {
        let local_services = get_all_local_service(filter_system).await?;
        Ok(devkit_service_infos_to_local(local_services))
    }

    fn broadcast_service_on_or_off(&self, service_info: ServiceInfo, is_on: bool) {
        x_core::spawn(async move {
            let remote_service = local_service_info_to_remote(service_info);
            // 获取 群发消息
            let conn_ids = get_all_conn_id().await;

            if conn_ids.is_err() {
                error!("获取所有的连接节点id 失败, 停止 通知...");
                return;
            }
            info!("获取所有连接节点...");
            for conn_id in conn_ids.unwrap().conn_id {
                let node_addr = conn_id_to_addr(conn_id);
                info!("通知节点 {} 服务状态更新...", node_addr);
                for _ in 0..3 {
                    info!(
                        "开始获取 connn_id：{} 获取所有连接节点...",
                        conn_id_to_addr(conn_id)
                    );
                    let channel_id_info = get_channel_id_by_conn_id(conn_id).await;

                    info!("返回 connn_id 获取所有连接节点...");
                    if channel_id_info.is_err() {
                        info!("获取节点 {} 的 channel 失败，10ms后重试...", &node_addr);
                        sleep(10).await;
                        continue;
                    }
                    let channel_id_info = channel_id_info.unwrap();
                    if channel_id_info.channel_id.is_none() {
                        info!("获取节点 {} 无连接，停止通知...", &node_addr);
                        break;
                    }
                    let channel_id = channel_id_info.channel_id.unwrap();

                    let ret = if is_on {
                        x_com_service_0_0_1::remote_service_on_with_channel_id(
                            channel_id,
                            &remote_service,
                        )
                        .await
                    } else {
                        x_com_service_0_0_1::remote_service_off_with_channel_id(
                            channel_id,
                            &remote_service,
                        )
                        .await
                    };

                    if ret.is_ok() {
                        info!("通知 {}，服务完成...", &node_addr);
                    } else {
                        info!(
                            "通知 {}， 服务完成，但是对方返回失败信息：{:?}...",
                            &node_addr,
                            ret.err()
                        );
                    }
                    break;
                }
            }
        });
    }

    pub async fn build_channel(
        &self,
        _ctx: &Context,
        param: BuildChannelRequest,
    ) -> x_core::Result<()> {
        let node_id = make_node_id(&param.node_ip, param.port as u16);
        let status = build_channel(node_id).await;
        if status.is_erorr() {
            Err(status)
        } else {
            Ok(())
        }
    }

    pub async fn load_service(
        &self,
        _ctx: &Context,
        param: LoadServiceRequest,
    ) -> x_core::Result<()> {
        println!("load_service = {:?}", param);
        let load_param = xComLib::LoadServiceRequest::new(
            param.dxc_name,
            param.dxc_version,
            param.service_name,
            param.only_build_ipc,
            param.config,
        );
        //
        let status = x_api::xport::load_service(&load_param).await;
        if status.is_erorr() {
            Err(status)
        } else {
            Ok(())
        }
    }

    pub async fn unload_service(
        &self,
        _ctx: &Context,
        param: UnloadServiceRequest,
    ) -> x_core::Result<()> {
        let unload_param = xComLib::UnloadServiceRequest::new(
            param.dxc_name,
            param.dxc_version,
            param.service_name,
        );
        let status = x_api::xport::unload_service(&unload_param).await;
        if status.is_erorr() {
            Err(status)
        } else {
            Ok(())
        }
    }

    pub async fn get_service_list(&self, _ctx: &Context) -> x_core::Result<ServiceInfos> {
        self.get_service_list_with_filter(false).await
    }

    pub async fn channel_connected(
        &self,
        _ctx: &Context,
        param: ChannelEvent,
    ) -> x_core::Result<()> {
        info!("新增连接...");

        let xrpc_port = get_xrpc_port().unwrap();

        let local_services = get_all_local_service(true).await?;

        let node_info = x_com_service_0_0_1::NodeInfo {
            service_infos: devkit_service_infos_to_remote(local_services),
            xrpc_port,
            cached_size: CachedSize::new(),
        };
        //
        let remote_node_local_service =
            x_com_service_0_0_1::sync_xrpc_to_remote_with_channel_id(param.channel_id, &node_info)
                .await?;

        let devkit_servics =
            remote_service_infos_to_devkit(remote_node_local_service.service_infos);
        // 将channel 添加到 远程服务中
        let status = add_channel_id_to_remote_services(param.channel_id, devkit_servics).await;
        if status.is_erorr() {
            Err(status)
        } else {
            Ok(())
        }
    }

    pub async fn channel_disconnected(
        &self,
        _ctx: &Context,
        param: ChannelEvent,
    ) -> x_core::Result<()> {
        info!("channel = {} 关闭 ...", param.channel_id);
        Ok(())
    }
    /**
     *
     */
    pub async fn sync_xrpc_to_remote(
        &self,
        ctx: &Context,
        param: NodeInfo,
    ) -> x_core::Result<ServiceInfos> {
        // 设置 channel xrpc 端口号
        let status = set_channel_xrpc_port(ctx.channel_id, param.xrpc_port).await;
        if status.is_erorr() {
            info!(
                "设置 channel {} 端口号失败：{}",
                ctx.channel_id, status.err_msg
            );
            return Err(status);
        }
        let devkit_service_infos = local_service_infos_to_devkit(param.service_infos);

        info!("sync_xrpc_to_remote channle_id = {}", ctx.channel_id);

        let status = add_channel_id_to_remote_services(ctx.channel_id, devkit_service_infos).await;

        if status.is_erorr() {
            return Err(status);
        }
        // 返回本地信息给对方
        self.get_service_list_with_filter(true).await
    }

    pub async fn local_service_on(
        &self,
        _ctx: &Context,
        service_info: ServiceInfo,
    ) -> x_core::Result<()> {
        info!("{:?} 上线 ", service_info);

        self.broadcast_service_on_or_off(service_info, true);
        Ok(())
    }
    pub async fn local_service_off(
        &self,
        _ctx: &Context,
        service_info: ServiceInfo,
    ) -> x_core::Result<()> {
        info!("{:?} 下线 ", service_info);
        self.broadcast_service_on_or_off(service_info, false);
        Ok(())
    }
    pub async fn remote_service_on(
        &self,
        ctx: &Context,
        service_info: ServiceInfo,
    ) -> x_core::Result<()> {
        info!("{:?} 通知 {:?} 上线", ctx.sender_service_key, service_info);

        let devkit_service_info = local_service_info_to_devkit(service_info);

        let mut devkit_services_info = xComLib::ServiceInfos::default();

        devkit_services_info.service_infos.push(devkit_service_info);

        let status = add_channel_id_to_remote_services(ctx.channel_id, devkit_services_info).await;

        if status.is_erorr() {
            error!("通知添加 channel_id 失败");
        }

        Ok(())
    }
    pub async fn remote_service_off(
        &self,
        ctx: &Context,
        service_info: ServiceInfo,
    ) -> x_core::Result<()> {
        info!("{:?} 通知 {:?} 下线", ctx.sender_service_key, service_info);

        let devkit_service_info = local_service_info_to_devkit(service_info);

        let status =
            remove_remote_services_all_channel_id(ctx.channel_id, devkit_service_info).await;

        if status.is_erorr() {
            error!("通知删除 channel_id 失败");
        }

        Ok(())
    }
}
